//dynamic planar fluid with lighting

#pragma once
#include "BaseShader.h"
#include "Light.h"
#include "Camera.h"
#include "N_OF_LIGHTS.h"

using namespace std;
using namespace DirectX;

class TerrainShader : public BaseShader
{
	//buffers types
	//multiples of 16: 16,32,48,64,80,96,112,128,144,160,176,192,208,224,240,256,272,288,304,320
	struct LightBuffer//lights description
	{
		XMFLOAT4 lightPosition[N_OF_LIGHTS];
		XMFLOAT4 specularColour[N_OF_LIGHTS];
		XMFLOAT4 diffuseColour[N_OF_LIGHTS];
		XMFLOAT4 ambientColour[N_OF_LIGHTS];
		XMFLOAT4 specularPower[N_OF_LIGHTS];//waste of bytes but padding every time n of lights changes is hard
		XMFLOAT4 fallOffDistance[N_OF_LIGHTS];//^^
	};

	struct TessellationBuffer//tessellation factor
	{
		float tessellationFactor;
		XMFLOAT3 padding;
	};

	struct ManipBuffer//data for vertex manipulation/orientation
	{
		XMFLOAT3 cameraPosition;
		int size;//size of patch
	};

public:
	TerrainShader(ID3D11Device* device, HWND hwnd);
	~TerrainShader();

	void SetShaderParameters(ID3D11DeviceContext* deviceContext,
		const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		ID3D11ShaderResourceView* heightMap, ID3D11ShaderResourceView* normalMap, ID3D11ShaderResourceView* texture,
		Light* lights[N_OF_LIGHTS], Camera* camera,	float tessFactor, int size);

	void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

	void releaseTextures(ID3D11DeviceContext* deviceContext);

private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
	void InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename);
	ID3D11Buffer* m_matrixBuffer;
	ID3D11SamplerState* m_sampleState;

	//buffers
	ID3D11Buffer* lightBfr;
	ID3D11Buffer* manipBfr;
	ID3D11Buffer* tessellationBfr;
};