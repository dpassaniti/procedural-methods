// draw noise to texture
#ifndef _NoiseShader_H_
#define _NoiseShader_H_

#include "BaseShader.h"

using namespace std;
using namespace DirectX;

class NoiseShader : public BaseShader
{
	struct ParamsBufferType
	{
		float frequency;
		float amplitude;
		int octaves;
		float pad0;
		XMFLOAT3 translation;
		float pad1;
	};

public:

	NoiseShader(ID3D11Device* device, HWND hwnd, WCHAR* psPath);
	~NoiseShader();

	void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		float frequency, float amplitude, int octaves, float translation[3]);
	void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

private:
	void InitShader(WCHAR*, WCHAR*);

private:
	ID3D11Buffer* m_matrixBuffer;
	ID3D11Buffer* paramsBuffer;
};

#endif