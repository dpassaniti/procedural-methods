//4 control points patch list for tessellation
//represents a plane made of [resoluition] quads of height and width [qSize] parallel to the xz plane
#pragma once
#include "BaseMesh.h"

using namespace DirectX;

class QuadCPPatch : public BaseMesh
{

public:
	QuadCPPatch(ID3D11Device* device, WCHAR* textureFilename, int resolution, int qSize);
	~QuadCPPatch();

	void SendData(ID3D11DeviceContext*);

protected:
	void InitBuffers(ID3D11Device* device);
	int m_resolution;
	int size;

};