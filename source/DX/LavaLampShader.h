//dim a texture
#pragma once
#include "BaseShader.h"

using namespace std;
using namespace DirectX;

class LavaLampShader : public BaseShader
{
	struct TimeBuffer//time
	{
		float time;
		XMFLOAT3 padding;
	};

	struct CorrectionBuffer//colour correction for texture
	{
		XMFLOAT4 colourCorrection;
	};

public:

	LavaLampShader(ID3D11Device* device, HWND hwnd);
	~LavaLampShader();

	void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		ID3D11ShaderResourceView* texture, XMFLOAT4 colourCorrection, float time);

	void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

	void releaseTextures(ID3D11DeviceContext * deviceContext);

private:
	void InitShader(WCHAR*, WCHAR*);

private:
	ID3D11Buffer* m_matrixBuffer;
	ID3D11SamplerState* m_sampleState;
	ID3D11Buffer* timeBfr;
	ID3D11Buffer* correctionBfr;
};