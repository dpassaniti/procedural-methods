//dynamic planar fluid with lighting

#include "TerrainShader.h"

TerrainShader::TerrainShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd)
{
	InitShader(L"../shaders/terrain_vs.hlsl", L"../shaders/terrain_hs.hlsl", L"../shaders/terrain_ds.hlsl", L"../shaders/terrain_ps.hlsl");
}

TerrainShader::~TerrainShader()
{
	// Release the sampler state.
	if (m_sampleState)
	{
		m_sampleState->Release();
		m_sampleState = 0;
	}

	// Release the matrix constant buffer.
	if (m_matrixBuffer)
	{
		m_matrixBuffer->Release();
		m_matrixBuffer = 0;
	}

	// Release the layout.
	if (m_layout)
	{
		m_layout->Release();
		m_layout = 0;
	}

	// Release the constant buffers.
	if (lightBfr)
	{
		lightBfr->Release();
		lightBfr = 0;
	}
	if (manipBfr)
	{
		manipBfr->Release();
		manipBfr = 0;
	}
	if (tessellationBfr)
	{
		tessellationBfr->Release();
		tessellationBfr = 0;
	}

	//Release base shader components
	BaseShader::~BaseShader();
}

void TerrainShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename)
{
	D3D11_BUFFER_DESC matrixBufferDesc, lightBfrDesc, manipBfrDesc, tessellationBfrDesc;
	D3D11_SAMPLER_DESC samplerDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);

	// Create a texture sampler state description.
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	// Create the texture sampler state.
	m_device->CreateSamplerState(&samplerDesc, &m_sampleState);

	//setup light buffer
	// Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail.
	lightBfrDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBfrDesc.ByteWidth = sizeof(LightBuffer);
	lightBfrDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBfrDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBfrDesc.MiscFlags = 0;
	lightBfrDesc.StructureByteStride = 0;
	//create the constant buffer pointer
	m_device->CreateBuffer(&lightBfrDesc, NULL, &lightBfr);

	//setup manip buffer
	manipBfrDesc.Usage = D3D11_USAGE_DYNAMIC;
	manipBfrDesc.ByteWidth = sizeof(ManipBuffer);
	manipBfrDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	manipBfrDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	manipBfrDesc.MiscFlags = 0;
	manipBfrDesc.StructureByteStride = 0;
	//create the constant buffer pointer
	m_device->CreateBuffer(&manipBfrDesc, NULL, &manipBfr);

	//setup tessellation buffer
	tessellationBfrDesc.Usage = D3D11_USAGE_DYNAMIC;
	tessellationBfrDesc.ByteWidth = sizeof(TessellationBuffer);
	tessellationBfrDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	tessellationBfrDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	tessellationBfrDesc.MiscFlags = 0;
	tessellationBfrDesc.StructureByteStride = 0;
	// Create the constant buffer pointer
	m_device->CreateBuffer(&tessellationBfrDesc, NULL, &tessellationBfr);
}

void TerrainShader::InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename)
{
	// InitShader must be overwritten and it will load both vertex and pixel shaders + setup buffers
	InitShader(vsFilename, psFilename);

	// Load other required shaders.
	loadHullShader(hsFilename);
	loadDomainShader(dsFilename);
}

void TerrainShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,
	const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
	ID3D11ShaderResourceView* heightMap, ID3D11ShaderResourceView* normalMap, ID3D11ShaderResourceView* texture,
	Light* lights[N_OF_LIGHTS], Camera* camera,	float tessFactor, int size)
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	XMMATRIX tworld, tview, tproj;

	//buffer pointers
	MatrixBufferType* dataPtr;
	LightBuffer* lightPtr;
	ManipBuffer* manipPtr;
	TessellationBuffer* tessellationPtr;
	//slot
	unsigned int bufferNumber;

	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(world);
	tview = XMMatrixTranspose(view);
	tproj = XMMatrixTranspose(projection);

	//HULL SHADER DATA
	deviceContext->Map(tessellationBfr, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	tessellationPtr = (TessellationBuffer*)mappedResource.pData;
	tessellationPtr->tessellationFactor = tessFactor;
	deviceContext->Unmap(tessellationBfr, 0);
	bufferNumber = 0;
	deviceContext->HSSetConstantBuffers(bufferNumber, 1, &tessellationBfr);

	//DOMAIN SHADER DATA
	// Lock the constant buffer so it can be written to.
	deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	// Copy the matrices into the constant buffer.
	dataPtr->world = tworld;// worldMatrix;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	// Unlock the constant buffer.
	deviceContext->Unmap(m_matrixBuffer, 0);
	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;
	// Now set the constant buffer in the vertex shader with the updated values.
	deviceContext->DSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

	//Send manip to domain shader
	deviceContext->Map(manipBfr, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	manipPtr = (ManipBuffer*)mappedResource.pData;
	manipPtr->cameraPosition = camera->GetPosition();
	manipPtr->size = size;
	deviceContext->Unmap(manipBfr, 0);
	bufferNumber = 1;
	deviceContext->DSSetConstantBuffers(bufferNumber, 1, &manipBfr);

	// Set shader texture resources
	deviceContext->DSSetShaderResources(0, 1, &heightMap);
	deviceContext->DSSetShaderResources(1, 1, &normalMap);

	//PIXEL SHADER DATA
	// Set shader texture resource in the pixel shader.
	deviceContext->PSSetShaderResources(0, 1, &texture);

	//Send light data to pixel shader
	deviceContext->Map(lightBfr, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	lightPtr = (LightBuffer*)mappedResource.pData;
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		lightPtr->lightPosition[i].x = lights[i]->GetPosition().x;
		lightPtr->lightPosition[i].y = lights[i]->GetPosition().y;
		lightPtr->lightPosition[i].z = lights[i]->GetPosition().z;
		lightPtr->lightPosition[i].w = 1.0f;//fill
		lightPtr->specularColour[i] = lights[i]->GetSpecularColour();
		lightPtr->diffuseColour[i] = lights[i]->GetDiffuseColour();
		lightPtr->ambientColour[i] = lights[i]->GetAmbientColour();
		lightPtr->specularPower[i].x = lights[i]->GetSpecularPower();
		lightPtr->fallOffDistance[i].x = lights[i]->GetFallOffDistance();
	}
	deviceContext->Unmap(lightBfr, 0);
	bufferNumber = 0;
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &lightBfr);
}

void TerrainShader::Render(ID3D11DeviceContext* deviceContext, int indexCount)
{
	// Set the sampler state in the domain shader.
	deviceContext->DSSetSamplers(0, 1, &m_sampleState);

	// Set the sampler state in the pixel shader.
	deviceContext->PSSetSamplers(0, 1, &m_sampleState);

	// Base render function.
	BaseShader::Render(deviceContext, indexCount);
}

void TerrainShader::releaseTextures(ID3D11DeviceContext* deviceContext)
{
	ID3D11ShaderResourceView* nullres = NULL;
	//unbind texture resources
	deviceContext->DSSetShaderResources(0, 1, &nullres);
	deviceContext->DSSetShaderResources(1, 1, &nullres);
	deviceContext->PSSetShaderResources(0, 1, &nullres);
}



