//dim a texture
#pragma once
#include "BaseShader.h"

using namespace std;
using namespace DirectX;

class DimShader : public BaseShader
{
	struct DimBuffer
	{
		float dimStrength;
		XMFLOAT3 padding;
	};
public:

	DimShader(ID3D11Device* device, HWND hwnd);
	~DimShader();

	void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		ID3D11ShaderResourceView* texture, float dimStrength);
	void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

private:
	void InitShader(WCHAR*, WCHAR*);

private:
	ID3D11Buffer* m_matrixBuffer;
	ID3D11SamplerState* m_sampleState;
	ID3D11Buffer* dimBfr;
};