//dim a texture
#include "LavaLampShader.h"

LavaLampShader::LavaLampShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd)
{
	InitShader(L"../shaders/lavaLamp_vs.hlsl", L"../shaders/lavaLamp_ps.hlsl");
}


LavaLampShader::~LavaLampShader()
{
	// Release the sampler state.
	if (m_sampleState)
	{
		m_sampleState->Release();
		m_sampleState = 0;
	}

	// Release the matrix constant buffer.
	if (m_matrixBuffer)
	{
		m_matrixBuffer->Release();
		m_matrixBuffer = 0;
	}

	// Release the layout.
	if (m_layout)
	{
		m_layout->Release();
		m_layout = 0;
	}

	if (timeBfr)
	{
		timeBfr->Release();
		timeBfr = 0;
	}

	if (correctionBfr)
	{
		correctionBfr->Release();
		correctionBfr = 0;
	}

	//Release base shader components
	BaseShader::~BaseShader();
}


void LavaLampShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename)
{
	D3D11_BUFFER_DESC matrixBufferDesc, timeBfrDesc, correctionBfrDesc;
	D3D11_SAMPLER_DESC samplerDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);

	// Create a texture sampler state description.
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	m_device->CreateSamplerState(&samplerDesc, &m_sampleState);

	// Setup the description for time buffer
	timeBfrDesc.Usage = D3D11_USAGE_DYNAMIC;
	timeBfrDesc.ByteWidth = sizeof(TimeBuffer);
	timeBfrDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	timeBfrDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	timeBfrDesc.MiscFlags = 0;
	timeBfrDesc.StructureByteStride = 0;
	m_device->CreateBuffer(&timeBfrDesc, NULL, &timeBfr);

	// Setup the colour correctione buffer
	correctionBfrDesc.Usage = D3D11_USAGE_DYNAMIC;
	correctionBfrDesc.ByteWidth = sizeof(CorrectionBuffer);
	correctionBfrDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	correctionBfrDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	correctionBfrDesc.MiscFlags = 0;
	correctionBfrDesc.StructureByteStride = 0;
	m_device->CreateBuffer(&correctionBfrDesc, NULL, &correctionBfr);

}


void LavaLampShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &worldMatrix, const XMMATRIX &viewMatrix,
	const XMMATRIX &projectionMatrix, ID3D11ShaderResourceView* texture, XMFLOAT4 colourCorrection, float time)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	unsigned int bufferNumber;
	XMMATRIX tworld, tview, tproj;
	TimeBuffer* timePtr;
	CorrectionBuffer* correctionPtr;

	//VERTEX SHADER DATA
	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(worldMatrix);
	tview = XMMatrixTranspose(viewMatrix);
	tproj = XMMatrixTranspose(projectionMatrix);
	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	// Copy the matrices into the constant buffer.
	dataPtr->world = tworld;// worldMatrix;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	// Unlock the constant buffer.
	deviceContext->Unmap(m_matrixBuffer, 0);
	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;
	// Now set the constant buffer in the vertex shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

	//send time buffer data to vs
	deviceContext->Map(timeBfr, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	timePtr = (TimeBuffer*)mappedResource.pData;
	timePtr->time = time;
	timePtr->padding = { 0, 0, 0 };
	deviceContext->Unmap(timeBfr, 0);
	bufferNumber = 1;
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &timeBfr);

	// Set shader texture resource in vs
	deviceContext->VSSetShaderResources(0, 1, &texture);

	//PIXEL SHADER DATA
	// Set shader texture resource in the pixel shader.
	deviceContext->PSSetShaderResources(0, 1, &texture);

	//send time buffer data to vs
	deviceContext->Map(correctionBfr, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	correctionPtr = (CorrectionBuffer*)mappedResource.pData;
	correctionPtr->colourCorrection = colourCorrection;
	deviceContext->Unmap(correctionBfr, 0);
	bufferNumber = 0;
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &correctionBfr);
}

void LavaLampShader::Render(ID3D11DeviceContext* deviceContext, int indexCount)
{
	// Set the sampler state in the pixel shader.
	deviceContext->PSSetSamplers(0, 1, &m_sampleState);

	// Base render function.
	BaseShader::Render(deviceContext, indexCount);
}

void LavaLampShader::releaseTextures(ID3D11DeviceContext* deviceContext)
{
	ID3D11ShaderResourceView* nullres = NULL;
	//unbind texture resources
	deviceContext->VSSetShaderResources(0, 1, &nullres);
	deviceContext->PSSetShaderResources(0, 1, &nullres);
}

