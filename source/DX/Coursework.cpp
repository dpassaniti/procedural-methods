#include "Coursework.h"

Coursework::Coursework(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input *in) : BaseApplication(hinstance, hwnd, screenWidth, screenHeight, in)
{
	srand(std::time(NULL));

	//init vars
	sWidth = screenWidth;
	sHeight = screenHeight;
	wireframe = false;
	tessellation = 64;
	dimStrength = 0.6;
	currentLight = 0;
	time = 0.0f;
	newNoise = true;
	seeNoise = false;
	terrainSize = 10;

	//reposition camera
	m_Camera->SetPosition(50.0f, 125.0f, 50.0f);	
	m_Camera->SetRotation(90, 0.0f, 0.0f);

	//create and init lights
	lightZero = new Light();
	lightZero->SetPosition(LIGHT0_POSITION);
	lightZero->SetLookAt(50.0f, 0.0f, 50.0f);
	lightZero->SetAmbientColour(0.5f, 0.5f, 0.5f, 0.5f);
	lightZero->SetDiffuseColour(LIGHT0_COLOUR);
	lightZero->SetSpecularColour(1.0f, 1.0f, 1.0f, 1.0f);
	lightZero->SetSpecularPower(20);
	lightZero->SetFallOffDistance(70);	
	
	lightOne = new Light();
	lightOne->SetPosition(LIGHT1_POSITION);
	lightOne->SetLookAt(50.0f, 0.0f, 50.0f);
	lightOne->SetAmbientColour(0.5f, 0.5f, 0.5f, 0.5f);
	lightOne->SetDiffuseColour(LIGHT1_COLOUR);
	lightOne->SetSpecularColour(1.0f, 1.0f, 1.0f, 1.0f);
	lightOne->SetSpecularPower(80);
	lightOne->SetFallOffDistance(50);

	lights[0] = lightZero;
	lights[1] = lightOne;

	//record light fallOffs
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		fallOffs[i] = lights[i]->GetFallOffDistance();
	}

	//create secondary render targets
	//glow post process
	//downsampled target to save previous frame's state
	previousGlow = new RenderTexture(m_Direct3D->GetDevice(), screenWidth / 2, screenHeight / 2, SCREEN_NEAR, SCREEN_DEPTH);
	previousGlow->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);//clear this to transparency
	//2 downsampled targets for multiple post process passes
	glowTargetA = new RenderTexture(m_Direct3D->GetDevice(), screenWidth / 2, screenHeight / 2, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(glowTargetA);
	glowTargetB = new RenderTexture(m_Direct3D->GetDevice(), screenWidth / 2, screenHeight / 2, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(glowTargetB);
	//noise targets
	noiseTarget = new RenderTexture(m_Direct3D->GetDevice(), NOISE_TEX_RES, NOISE_TEX_RES, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(noiseTarget);
	normalsTarget = new RenderTexture(m_Direct3D->GetDevice(), NOISE_TEX_RES, NOISE_TEX_RES, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(normalsTarget);
	detailsTarget = new RenderTexture(m_Direct3D->GetDevice(), NOISE_TEX_RES, NOISE_TEX_RES, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(detailsTarget);
	waterNormalsTarget = new RenderTexture(m_Direct3D->GetDevice(), NOISE_TEX_RES, NOISE_TEX_RES, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(waterNormalsTarget);
	blurredDetailsTarget = new RenderTexture(m_Direct3D->GetDevice(), NOISE_TEX_RES, NOISE_TEX_RES, SCREEN_NEAR, SCREEN_DEPTH);
	renderTargets.push_back(blurredDetailsTarget);

	//create textures
	//skybox
	skyboxTex = new Texture(m_Direct3D->GetDevice(), L"../res/seamlesstexturesdotdeviantart.jpg");
	textures.push_back(skyboxTex);

	//create meshes
	skybox = new Skybox(m_Direct3D->GetDevice(), L"../res/DefaultDiffuse.png", 8);
	meshes.push_back(skybox);
	lamp = new SphereTriList(m_Direct3D->GetDevice(), L"../res/DefaultDiffuse.png", 32);
	meshes.push_back(lamp);
	terrain = new QuadCPPatch(m_Direct3D->GetDevice(), L"../res/DefaultDiffuse.png", terrainSize, terrainSize);
	meshes.push_back(terrain);
	water = new QuadCPPatch(m_Direct3D->GetDevice(), L"../res/DefaultDiffuse.png", terrainSize, terrainSize);
	meshes.push_back(water);
	//ortho mesh for 2d rendering
	noiseTex = new OrthoMesh(m_Direct3D->GetDevice(), screenWidth, screenHeight);
	meshes.push_back(noiseTex);
	//ortho mesh for post process
	glowFilter = new OrthoMesh(m_Direct3D->GetDevice(), screenWidth, screenHeight);
	meshes.push_back(glowFilter);

	//create shaders
	textureS = new TextureShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(textureS);
	noiseS = new NoiseShader(m_Direct3D->GetDevice(), hwnd, L"../shaders/noise_ps.hlsl");
	shaders.push_back(noiseS);
	noiseDetailS = new NoiseShader(m_Direct3D->GetDevice(), hwnd, L"../shaders/noiseDetail_ps.hlsl");
	shaders.push_back(noiseDetailS);
	noiseNormalsS = new NoiseNormalsShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(noiseNormalsS);
	hBlurS = new HorizontalBlurShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(hBlurS);
	vBlurS = new VerticalBlurShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(vBlurS);
	dimS = new DimShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(dimS);
	lavaLampS = new LavaLampShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(lavaLampS);
	terrainS = new TerrainShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(terrainS);
	waterS = new WaterShader(m_Direct3D->GetDevice(), hwnd);
	shaders.push_back(waterS);

	//generate water normal maps and details
	XMMATRIX worldMatrix, orthoMatrix, baseViewMatrix;;
	// Get the world, get ortho matrix and base view matrix for 2d rendering
	m_Direct3D->GetWorldMatrix(worldMatrix);
	m_Direct3D->GetOrthoMatrix(orthoMatrix);
	m_Camera->GetBaseViewMatrix(baseViewMatrix);

	//render water normals
	frequency = 0.03;
	amplitude = 0.5;
	octaves = 5;
	translation[0] = rand() % 256;
	translation[1] = rand() % 256;
	translation[2] = rand() % 256;
	generateNormalMap(&worldMatrix, &baseViewMatrix, &orthoMatrix, waterNormalsTarget, frequency, amplitude, octaves, translation);	

	//set render target to back buffer
	m_Direct3D->SetBackBufferRenderTarget();
	//reset screen size
	m_Direct3D->ResetViewport();

	//starting fbm parameters for terrain
	frequency = 0.007f;//[0.001, 0.01]
	amplitude = 20.0f;//[1,35]
	octaves = 7;
	translation[0] = 0.0f;
	translation[1] = 0.0f;
	translation[2] = 0.0f;
}

bool Coursework::Frame()
{
	bool result;

	result = BaseApplication::Frame();
	if (!result)
	{
		return false;
	}

	//update vars
	time += m_Timer->GetTime();

	// Render the graphics.
	result = Render();
	if (!result)
	{
		return false;
	}

	return true;
}

bool Coursework::Render()
{
	XMMATRIX worldMatrix, viewMatrix, projectionMatrix, orthoMatrix, baseViewMatrix;;

	// Clear the scene.
	m_Direct3D->BeginScene(0.1f, 0.1f, 0.1f, 1.0f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Get the world, view, projection, and ortho matrices from the camera and Direct3D objects.
	m_Direct3D->GetWorldMatrix(worldMatrix);
	m_Camera->GetViewMatrix(viewMatrix);
	m_Direct3D->GetProjectionMatrix(projectionMatrix);
	////get ortho matrix and base view matrix for 2d rendering
	m_Direct3D->GetOrthoMatrix(orthoMatrix);
	m_Camera->GetBaseViewMatrix(baseViewMatrix);

	//render skybox
	m_Direct3D->TurnZBufferOff();
	//translate to camera position
	worldMatrix = DirectX::XMMatrixTranslation(m_Camera->GetPosition().x, m_Camera->GetPosition().y, m_Camera->GetPosition().z);
	//render skybox with texture shader
	skybox->SendData(m_Direct3D->GetDeviceContext());
	textureS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, skyboxTex->GetTexture());
	textureS->Render(m_Direct3D->GetDeviceContext(), skybox->GetIndexCount());
	//reset world matrix
	m_Direct3D->GetWorldMatrix(worldMatrix);
	
	m_Direct3D->TurnZBufferOn();
	
	//change terrain
	if (newNoise)
	{
		terrainS->releaseTextures(m_Direct3D->GetDeviceContext());
		lavaLampS->releaseTextures(m_Direct3D->GetDeviceContext());

		//render terrain textures
		generateNoiseTexture(&worldMatrix, &baseViewMatrix, &orthoMatrix, noiseTarget, frequency, amplitude, octaves, translation);
		generateNormalMap(&worldMatrix, &baseViewMatrix, &orthoMatrix, normalsTarget, frequency, amplitude, octaves, translation);
		newNoise = false;

		//render details texture
		generateDetailTexture(&worldMatrix, &baseViewMatrix, &orthoMatrix, detailsTarget, frequency, amplitude, octaves, translation);
		blurTarget(&worldMatrix, &baseViewMatrix, &orthoMatrix, detailsTarget, blurredDetailsTarget);

		//randomise params for next iteration
		frequency = 0.001 + ((float) rand()) / ((float) (RAND_MAX / 0.009));
		amplitude = 1 + rand() % 35;
		translation[0] = rand() % 256;
		translation[1] = rand() % 256;
		translation[2] = rand() % 256;

		//set render target to back buffer
		m_Direct3D->SetBackBufferRenderTarget();
		//reset screen size
		m_Direct3D->ResetViewport();
	}
	if (seeNoise)
	{
		//render height map to screen
		noiseTex->SendData(m_Direct3D->GetDeviceContext());
		textureS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, baseViewMatrix, orthoMatrix, noiseTarget->GetShaderResourceView());
		textureS->Render(m_Direct3D->GetDeviceContext(), noiseTex->GetIndexCount());
	}
	
	//render terrain
	terrain->SendData(m_Direct3D->GetDeviceContext());
	terrainS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, viewMatrix, projectionMatrix,
		noiseTarget->GetShaderResourceView(), normalsTarget->GetShaderResourceView(), blurredDetailsTarget->GetShaderResourceView(),
		lights, m_Camera, tessellation, terrainSize);
	terrainS->Render(m_Direct3D->GetDeviceContext(), terrain->GetIndexCount());

	//don't show glow when looking at height map
	if (!seeNoise)
	{
		//post process lamps to create glow
		//render lights to texture
		lightsToTexture(&worldMatrix, &viewMatrix, &projectionMatrix);
		//Turn off the Z buffer to begin all 2D rendering.
		m_Direct3D->TurnZBufferOff();
		//turn on alpha blending
		m_Direct3D->TurnOnAlphaBlending();
		//blend with previous frame and save result for next iteration
		addGlowTrail(&worldMatrix, &baseViewMatrix, &orthoMatrix);
		//apply horizontal blur
		hBlur(&worldMatrix, &baseViewMatrix, &orthoMatrix);
		//apply vertical blur
		vBlur(&worldMatrix, &baseViewMatrix, &orthoMatrix);
		//render final filter on top of the scene
		drawGlowFilter(&worldMatrix, &baseViewMatrix, &orthoMatrix);
		//turn alpha blending back off
		m_Direct3D->TurnOffAlphaBlending();
		//now that all 2d rendering is done Turn the Z buffer back on
		m_Direct3D->TurnZBufferOn();
	}

	//render lights
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		//scale translate to light position
		worldMatrix = DirectX::XMMatrixTranslation(lights[i]->GetPosition().x, lights[i]->GetPosition().y, lights[i]->GetPosition().z);
		//send geometry data and render with texture shader
		lamp->SendData(m_Direct3D->GetDeviceContext());
		lavaLampS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, viewMatrix, projectionMatrix,
			detailsTarget->GetShaderResourceView(), lights[i]->GetDiffuseColour(), time);
		lavaLampS->Render(m_Direct3D->GetDeviceContext(), lamp->GetIndexCount());
		//reset world matrix
		m_Direct3D->GetWorldMatrix(worldMatrix);
	}

	//turn on blend mode 2 (see d3d.cpp for blend state details)
	m_Direct3D->TurnOnAlphaBlending2();
	//render water
	water->SendData(m_Direct3D->GetDeviceContext());
	waterS->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, viewMatrix, projectionMatrix,
		waterNormalsTarget->GetShaderResourceView(), lights, m_Camera, 30, terrainSize, time / 50);
	waterS->Render(m_Direct3D->GetDeviceContext(), terrain->GetIndexCount());
	//turn off alpha blending
	m_Direct3D->TurnOffAlphaBlending();

	// Present the rendered scene to the screen.
	m_Direct3D->EndScene();
	return true;
}

void Coursework::generateNoiseTexture(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix, RenderTexture* target,
	float frequency, float amplitude, int octaves, float translation[3])
{
	// Set the render target
	target->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	target->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);
	
	//render noise to texture
	noiseTex->SendData(m_Direct3D->GetDeviceContext());
	noiseS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		frequency, amplitude, octaves, translation);
	noiseS->Render(m_Direct3D->GetDeviceContext(), noiseTex->GetIndexCount());
}

void Coursework::generateDetailTexture(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix, RenderTexture* target,
	float frequency, float amplitude, int octaves, float translation[3])
{
	// Set the render target
	target->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	target->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);

	//render noise to texture
	noiseTex->SendData(m_Direct3D->GetDeviceContext());
	noiseDetailS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		frequency, amplitude, octaves, translation);
	noiseDetailS->Render(m_Direct3D->GetDeviceContext(), noiseTex->GetIndexCount());
}

void Coursework::generateNormalMap(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix, RenderTexture* target,
	float frequency, float amplitude, int octaves, float translation[3])
{
	// Set the render target
	target->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	target->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);

	//render noise to texture
	noiseTex->SendData(m_Direct3D->GetDeviceContext());
	noiseNormalsS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		frequency, amplitude, octaves, translation);
	noiseNormalsS->Render(m_Direct3D->GetDeviceContext(), noiseTex->GetIndexCount());
}

void Coursework::blurTarget(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix, RenderTexture * source, RenderTexture * target)
{
	//blur pass 1
	// Set the render target to gtA
	glowTargetA->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	glowTargetA->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);
	noiseTex->SendData(m_Direct3D->GetDeviceContext());
	hBlurS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		source->GetShaderResourceView(), sWidth);
	hBlurS->Render(m_Direct3D->GetDeviceContext(), noiseTex->GetIndexCount());

	//blur pass 2
	// Set the render target to target
	target->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	target->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);
	noiseTex->SendData(m_Direct3D->GetDeviceContext());
	vBlurS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetA->GetShaderResourceView(), sWidth);
	vBlurS->Render(m_Direct3D->GetDeviceContext(), noiseTex->GetIndexCount());
}

void Coursework::lightsToTexture(XMMATRIX* worldMatrix, XMMATRIX* viewMatrix, XMMATRIX* projectionMatrix)
{
	// Set the render target to be the downsampled target A
	glowTargetA->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	glowTargetA->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);
	
	//render lights
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		//translate to light position and add periodic floating
		*worldMatrix = DirectX::XMMatrixTranslation(lights[i]->GetPosition().x, lights[i]->GetPosition().y, lights[i]->GetPosition().z);
		//send geometry data and render with texture shader
		lamp->SendData(m_Direct3D->GetDeviceContext());
		lavaLampS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *viewMatrix, *projectionMatrix,
			detailsTarget->GetShaderResourceView(), lights[i]->GetDiffuseColour(), time);
		lavaLampS->Render(m_Direct3D->GetDeviceContext(), lamp->GetIndexCount());
		//reset world matrix
		m_Direct3D->GetWorldMatrix(*worldMatrix);
	}
}

void Coursework::addGlowTrail(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix)
{
	//the render target is still glowTargetA
	//dim the previous frame and blend it with the current one
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	dimS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		previousGlow->GetShaderResourceView(), dimStrength);
	dimS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());

	//Save the current frame
	//Set the render target to be the previousGlow render to texture.
	previousGlow->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//save current frame with texture shader
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	textureS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetA->GetShaderResourceView());
	textureS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());
}

void Coursework::hBlur(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix)
{
	//Set the render target to be the down sampled render to texture B.
	glowTargetB->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	glowTargetB->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);

	//render glowTargetA on glowTargetB applying horizontal blur
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	hBlurS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetA->GetShaderResourceView(), sWidth);
	hBlurS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());
}

void Coursework::vBlur(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix)
{
	//Set the render target to be the down sampled render to texture A.
	glowTargetA->SetRenderTarget(m_Direct3D->GetDeviceContext());
	//clear to black with aplha = 0
	glowTargetA->ClearRenderTarget(m_Direct3D->GetDeviceContext(), CC);

	//render glowTargetA on glowTargetB applying horizontal blur
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	vBlurS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetB->GetShaderResourceView(), sWidth);
	vBlurS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());
}

void Coursework::drawGlowFilter(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix)
{
	//reset screen size
	m_Direct3D->ResetViewport();
	//set render target to back buffer
	m_Direct3D->SetBackBufferRenderTarget();
	//render final filter to screen
	glowFilter->SendData(m_Direct3D->GetDeviceContext());
	textureS->SetShaderParameters(m_Direct3D->GetDeviceContext(), *worldMatrix, *baseViewMatrix, *orthoMatrix,
		glowTargetA->GetShaderResourceView());
	textureS->Render(m_Direct3D->GetDeviceContext(), glowFilter->GetIndexCount());
}

void Coursework::HandleInput(float frameTime)
{
	BaseApplication::HandleInput(frameTime);

	//wireframe on/off
	if (m_Input->isKeyDown(VK_SPACE))
	{
		if (wireframe)
		{
			m_Direct3D->TurnOffWireframe();
		}
		else
		{
			m_Direct3D->TurnOnWireframe();
		}
		m_Input->SetKeyUp(VK_SPACE);
		wireframe = !wireframe;
	}

	//change selected light
	if (m_Input->isKeyDown('1'))
	{
		currentLight = 0;
		m_Input->SetKeyUp('1');
	}
	if (m_Input->isKeyDown('2'))
	{
		currentLight = 1;
		m_Input->SetKeyUp('2');
	}

	//toggle on/off selected light (off = falloff distance = 0)
	if (m_Input->isKeyDown(VK_TAB))
	{
		if (lights[currentLight]->GetFallOffDistance() != 0)
		{
			fallOffs[currentLight] = lights[currentLight]->GetFallOffDistance();
			lights[currentLight]->SetFallOffDistance(0);
		}
		else
			lights[currentLight]->SetFallOffDistance(fallOffs[currentLight]);

		m_Input->SetKeyUp(VK_TAB);
	}

	//toggle seeNoise
	if (m_Input->isKeyDown(VK_CONTROL))
	{
		seeNoise = !seeNoise;
		m_Input->SetKeyUp(VK_CONTROL);
	}
	//generate new noise tex
	if (m_Input->isKeyDown(VK_RETURN))
	{
		newNoise = true;
		m_Input->SetKeyUp(VK_RETURN);
	}

	//control light positions of selected light with RTYFGH
	if (m_Input->isKeyDown('R'))// - Y
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x, lights[currentLight]->GetPosition().y - m_Timer->GetTime() * 10, lights[currentLight]->GetPosition().z);
	if (m_Input->isKeyDown('T'))// + Z
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x, lights[currentLight]->GetPosition().y, lights[currentLight]->GetPosition().z + m_Timer->GetTime() * 10);
	if (m_Input->isKeyDown('Y'))// + Y
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x, lights[currentLight]->GetPosition().y + m_Timer->GetTime() * 10, lights[currentLight]->GetPosition().z);
	if (m_Input->isKeyDown('F'))// - X
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x - m_Timer->GetTime() * 10, lights[currentLight]->GetPosition().y, lights[currentLight]->GetPosition().z);
	if (m_Input->isKeyDown('G'))// - Z
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x, lights[currentLight]->GetPosition().y, lights[currentLight]->GetPosition().z - m_Timer->GetTime() * 10);
	if (m_Input->isKeyDown('H'))// + X
		lights[currentLight]->SetPosition(lights[currentLight]->GetPosition().x + m_Timer->GetTime() * 10, lights[currentLight]->GetPosition().y, lights[currentLight]->GetPosition().z);

	//if shift increase variables, if no shift decrease
	if (m_Input->isKeyDown(VK_SHIFT))
	{
		//control tessellation density
		if (m_Input->isKeyDown('Z') && tessellation < 64)
		{
			tessellation += m_Timer->GetTime() * 10;
			if (tessellation > 64)
				tessellation = 64;
		}
		//control dim strength
		if (m_Input->isKeyDown('X') && dimStrength < 1.5)
		{
			dimStrength += m_Timer->GetTime();
		}
		//control falloff distance of current light
		if (m_Input->isKeyDown('C'))
			lights[currentLight]->SetFallOffDistance(lights[currentLight]->GetFallOffDistance() + 10 * m_Timer->GetTime());
	}
	else
	{
		//control tessellation density
		if (m_Input->isKeyDown('Z') && tessellation > 1)
		{
			tessellation -= m_Timer->GetTime() * 10;
			if (tessellation < 1)
				tessellation = 1;
		}
		//control dim strength
		if (m_Input->isKeyDown('X') && dimStrength > 0.4)
		{
			dimStrength -= m_Timer->GetTime();
			if (dimStrength < 0.4)
				dimStrength = 0.4;
		}
		//control falloff distance of current light if light is on
		if (m_Input->isKeyDown('C') && (lights[currentLight]->GetFallOffDistance() > 0))
		{
			lights[currentLight]->SetFallOffDistance(lights[currentLight]->GetFallOffDistance() - 10 * m_Timer->GetTime());
			if (lights[currentLight]->GetFallOffDistance() < 0)
			{
				lights[currentLight]->SetFallOffDistance(0);
			}
		}
	}
}

Coursework::~Coursework()
{
	//debug
	//ID3D11Debug* dbg(NULL);
	//m_Direct3D->GetDevice()->QueryInterface(__uuidof(ID3D11Debug), (void**)dbg);
	//if (dbg != nullptr)
	//{
	//	dbg->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	//	dbg = nullptr;
	//}
	
	// Run base application deconstructor
	BaseApplication::~BaseApplication();

	//release shaders, meshes, textures and vectors
	for (int i = 0; i < shaders.size(); ++i)
	{
		if (shaders[i])
		{
			delete shaders[i];
			shaders[i] = 0;
		}
	}
	for (int i = 0; i < meshes.size(); ++i)
	{
		if (meshes[i])
		{
			delete meshes[i];
			meshes[i] = 0;
		}
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
		{
			delete textures[i];
			textures[i] = 0;
		}
	}
	for (int i = 0; i < renderTargets.size(); ++i)
	{
		if (renderTargets[i])
		{
			delete renderTargets[i];
			renderTargets[i] = 0;
		}
	}
	std::vector<BaseShader*>().swap(shaders);
	std::vector<BaseMesh*>().swap(meshes);
	std::vector<Texture*>().swap(textures);
	std::vector<RenderTexture*>().swap(renderTargets);

	//free lights
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		if (lights[i])
		{
			delete lights[i];
			lights[i] = 0;
		}
	}
}