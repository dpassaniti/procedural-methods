#pragma once
//includes
#include <ctime>
#include <vector>
#include "baseapplication.h"
#include "Texture.h"
#include "Light.h"
#include "RenderTexture.h"

//shaders
#include "TextureShader.h"
#include "DimShader.h"
#include "HorizontalBlurShader.h"
#include "VerticalBlurShader.h"
#include "LavaLampShader.h"
#include "TerrainShader.h"
#include "WaterShader.h"
#include "NoiseShader.h"
#include "NoiseNormalsShader.h"

//meshes
#include "SphereTriList.h"
#include "OrthoMesh.h"
#include "Skybox.h"
#include "QuadCPPatch.h"

//defines
#include "N_OF_LIGHTS.h"

#define LIGHT0_POSITION 20.0f, 5.0f, 10.0f
#define LIGHT0_COLOUR 0.9f, 0.4f, 0.1f, 1.0f

#define LIGHT1_POSITION 70.0f, 15.0f, 40.0f
#define LIGHT1_COLOUR 0.3f, 0.8f, 0.3f, 1.0f

#define NOISE_TEX_RES 512//resolution for noise textures/render targets
#define CC 0.0f, 0.0f, 0.0f, 0.0f//clear colour for secondary render targets

class Coursework : public BaseApplication
{
public:
	Coursework(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input*);
	~Coursework();
	bool Frame();
	
	//texture generation & manipulation
	void generateNoiseTexture(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix, RenderTexture* target,
		float frequency, float amplitude, int octaves, float translation[3]);
	void generateDetailTexture(XMMATRIX * worldMatrix, XMMATRIX * baseViewMatrix, XMMATRIX * orthoMatrix, RenderTexture * target,
		float frequency, float amplitude, int octaves, float translation[3]);
	void generateNormalMap(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix, RenderTexture* target,
		float frequency, float amplitude, int octaves, float translation[3]);
	void blurTarget(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix, RenderTexture* source, RenderTexture* target);

	//glow post processing
	void lightsToTexture(XMMATRIX* worldMatrix, XMMATRIX* viewMatrix, XMMATRIX* projectionMatrix);
	void addGlowTrail(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix);
	void hBlur(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix);
	void vBlur(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix);	
	void drawGlowFilter(XMMATRIX* worldMatrix, XMMATRIX* baseViewMatrix, XMMATRIX* orthoMatrix);
	
private:
	void HandleInput(float frameTime);
	bool Render();

	//containers
	std::vector<BaseShader*> shaders;
	std::vector<BaseMesh*> meshes;
	std::vector<Texture*> textures;
	std::vector<RenderTexture*> renderTargets;

	//lights
	Light* lights[N_OF_LIGHTS];
	Light* lightZero;
	Light* lightOne;

	//shaders
	TextureShader* textureS;
	DimShader* dimS;
	LavaLampShader* lavaLampS;
	HorizontalBlurShader* hBlurS;
	VerticalBlurShader* vBlurS;
	TerrainShader* terrainS;
	NoiseShader* noiseS, *noiseDetailS;
	NoiseNormalsShader* noiseNormalsS;
	WaterShader* waterS;

	//meshes
	OrthoMesh* glowFilter;//post process
	OrthoMesh* noiseTex;//for 2d rendering of noise
	Skybox* skybox;
	SphereTriList* lamp;
	QuadCPPatch* terrain;
	QuadCPPatch* water;

	//textures
	Texture* lightTex;
	Texture* skyboxTex;

	//secondary render targets
	RenderTexture* glowTargetA;
	RenderTexture* glowTargetB;
	RenderTexture* previousGlow;
	RenderTexture* noiseTarget;
	RenderTexture* normalsTarget;
	RenderTexture* waterNormalsTarget;

	RenderTexture* detailsTarget;
	RenderTexture* blurredDetailsTarget;

	//flags and variables
	bool wireframe;//on\off
	float time;//for dynamic manipulation
	float tessellation;//tessellation factor, 1 to 64

	float fallOffs[N_OF_LIGHTS];//remember light fall off when turning it off
	bool newNoise;//generate new noise tex
	bool seeNoise;//display noise tex on screen
	float terrainSize;//size of terrain patch (used both for single quad and n of quads in x,z)
	int currentLight;//keep track of selected light
	float sWidth, sHeight;//screen dimensions
	float dimStrength;//brightness of glow effect
	 //fbm params
	float frequency, amplitude;
	int octaves;
	float translation[3];
};