//terrain vertex shader, simply passes on position to hull shader

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float3 position : POSITION;
};

OutputType main(InputType input)
{
	OutputType output;

	// Pass the vertex position and noraml into the hull shader.
	output.position = input.position;

	return output;
}