//pixel shader
//reduce brightness of texture

Texture2D texture0 : register(t0);
SamplerState Sampler0 : register(s0);

cbuffer DimBuffer: register(cb0)
{
	float dimStrength;
	float3 padding;
};

struct InputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

float4 main(InputType input) : SV_TARGET
{
	float4 finalColour;
	float4 textureColour;
	
	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	textureColour = texture0.Sample(Sampler0, input.tex);
	
	//dim the colour by interpolating with semi transparent black
	//change dimColour alpha and lerp weight for stronger/weaker glow effect
	//too low will cause a white overflow
	//too high will cause invisibility/flickering
	float4 dimColour = { 0.0f, 0.0f, 0.0f, 0.65f };
	finalColour = lerp(textureColour, dimColour, dimStrength);//0.4 to 1.5
	
	return finalColour;
}