// Terrain domain shader

Texture2D noisetex : register(t0);
Texture2D normalmap : register(t1);
SamplerState Sampler : register(s0);

cbuffer MatrixBuffer : register(cb0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer ManipBuffer : register(cb1)
{
	float3 cameraPosition;
	int size;
};

struct ConstantOutputType
{
	float edges[4] : SV_TessFactor;
	float inside[2] : SV_InsideTessFactor;
	float4 color : COLOR;
};

struct InputType
{
	float3 position : POSITION;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 position3D : TEXCOORD2;
	float3 viewDirection : TEXCOORD1;
};

[domain("quad")]
OutputType main(ConstantOutputType input, float2 uvwCoord : SV_DomainLocation, const OutputPatch<InputType, 4> patch)
{
	float3 vertexPosition;
	OutputType output;

	// Determine the position of the new vertex by interpolation
	//care!!!the ordering of control points depends on the order in which the quad verteces are drawn in the mesh
	float3 v1 = lerp(patch[0].position, patch[1].position, 1 - uvwCoord.y);
	float3 v2 = lerp(patch[2].position, patch[3].position, 1 - uvwCoord.y);
	vertexPosition = lerp(v1, v2, uvwCoord.x);

	//calculate texture coordinates based on new position and total size of patch
	//divide by size^2, assume size of a quad in cppatch = n of quads in one dimension of patch  
	output.tex.x = vertexPosition.x / (size*size);
	output.tex.y = -vertexPosition.z / (size*size);

	//MANIPULATION
	//displace and get new normal form maps
	float4 noiseDisplacement = noisetex.SampleLevel(Sampler, output.tex, 0.0f);
	vertexPosition.y += noiseDisplacement.x;
	float3 newNormal = normalmap.SampleLevel(Sampler, output.tex, 0.0f).xyz;

	//FINALISE OUTPUT
	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(float4(vertexPosition, 1.0f), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	// Calculate the normal vector against the world matrix only.
	output.normal = mul(newNormal, (float3x3)worldMatrix);
	// Normalize the normal vector.
	output.normal = normalize(output.normal);

	// world position of vertex
	output.position3D = mul(vertexPosition, worldMatrix);
	// Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
	output.viewDirection.x = cameraPosition.x - output.position3D.x;
	output.viewDirection.y = cameraPosition.y - output.position3D.y;
	output.viewDirection.z = cameraPosition.z - output.position3D.z;
	// Normalize the viewing direction vector.
	output.viewDirection = normalize(output.viewDirection);

	return output;
}

