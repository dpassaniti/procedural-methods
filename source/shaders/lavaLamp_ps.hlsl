// correct texture colour

Texture2D texture0 : register(t0);
SamplerState Sampler0 : register(s0);

cbuffer CorrectionBuffer : register(cb0)
{
	float4 colourCorrection;
};

struct InputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};


float4 main(InputType input) : SV_TARGET
{
	float4 textureColour;
	float4 finalColour;

	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	textureColour = texture0.Sample(Sampler0, input.tex);

	//blend texture colour with colour correction
	finalColour = lerp(textureColour, colourCorrection, 0.7);
	//brighten colour
	finalColour*=2;

	return saturate(finalColour);
}