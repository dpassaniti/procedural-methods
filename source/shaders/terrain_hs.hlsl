// Terrain Hull Shader
// Prepares control points for tessellation, uniform tessellation based on tessellationFactor

cbuffer TessellationBuffer : register(cb)
{
	float tessellationFactor;
	float3 padding;
};

struct InputType
{
	float3 position : POSITION;
};

struct ConstantOutputType
{
	float edges[4] : SV_TessFactor;
	float inside[2] : SV_InsideTessFactor;
	float4 color : COLOR;

};

struct OutputType
{
	//float4 color : COLOR;
	float3 position : POSITION;
};

ConstantOutputType PatchConstantFunction(InputPatch<InputType, 4> inputPatch, uint patchId : SV_PrimitiveID)
{
	ConstantOutputType output;

	//Set the tessellation factors for the four edges of the quad.
	output.edges[0] = tessellationFactor;
	output.edges[1] = tessellationFactor;
	output.edges[2] = tessellationFactor;
	output.edges[3] = tessellationFactor;
	//Set the 2 tessellation factors for tessallating inside the quad.
	output.inside[0] = tessellationFactor;
	output.inside[1] = tessellationFactor;

	output.color = float4(1.0f, 0.0f, 0.7f, 1.0f);

	return output;
}


[domain("quad")]
[partitioning("fractional_even")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("PatchConstantFunction")]
OutputType main(InputPatch<InputType, 4> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
	OutputType output;

	// Set the position for this control point as the output position.
	output.position = patch[pointId].position;

	return output;
}