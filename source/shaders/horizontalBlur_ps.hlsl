Texture2D shaderTexture : register(t0);
SamplerState SampleType : register(s0);

struct InputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
    
	float2 texCoord1 : TEXCOORD1;
	float2 texCoord2 : TEXCOORD2;
	float2 texCoord3 : TEXCOORD3;
	float2 texCoord4 : TEXCOORD4;

	float2 texCoord5 : TEXCOORD5;

	float2 texCoord6 : TEXCOORD6;
	float2 texCoord7 : TEXCOORD7;
	float2 texCoord8 : TEXCOORD8;
	float2 texCoord9 : TEXCOORD9;

};

float4 main(InputType input) : SV_TARGET
{
	float weight0, weight1, weight2, weight3, weight4;
    float4 colour;

	weight0 = 0.2;
	weight1 = 0.2;
	weight2 = 0.2;
	weight3 = 0.2;
	weight4 = 0.2;

	// Initialize the colour to black.
    colour = float4(0.0f, 0.0f, 0.0f, 0.0f);

    // Add the horizontal pixels to the colour by the specific weight of each.
	colour += shaderTexture.Sample(SampleType, input.texCoord1) * weight4;
    colour += shaderTexture.Sample(SampleType, input.texCoord2) * weight3;
    colour += shaderTexture.Sample(SampleType, input.texCoord3) * weight2;
    colour += shaderTexture.Sample(SampleType, input.texCoord4) * weight1;
   
	colour += shaderTexture.Sample(SampleType, input.texCoord5) * weight0;

	colour += shaderTexture.Sample(SampleType, input.texCoord6) * weight1;
	colour += shaderTexture.Sample(SampleType, input.texCoord7) * weight2;
	colour += shaderTexture.Sample(SampleType, input.texCoord8) * weight3;
	colour += shaderTexture.Sample(SampleType, input.texCoord8) * weight4;

	colour.a = 0.0f;

    return colour;
}
