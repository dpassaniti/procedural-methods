//water pixel shader

#define N_OF_LIGHTS 2//see N_OF_LIGHTS.h

cbuffer LightBuffer : register(cb0)
{
	float4 lightPosition[N_OF_LIGHTS];
	float4 specularColour[N_OF_LIGHTS];
	float4 diffuseColour[N_OF_LIGHTS];
	float4 ambientColour[N_OF_LIGHTS];
	float4 specularPower[N_OF_LIGHTS];
	float4 fallOffDistance[N_OF_LIGHTS];
};

struct InputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 position3D : TEXCOORD2;
	float3 viewDirection : TEXCOORD1;
};


float4 main(InputType input) : SV_TARGET
{
	float lightIntensity;//check if we need to affect value at all
	float4 specularIntensity;//perfect mirror has highest,which means smallest highlight
	float3 lightDir;//vector from light to vertex(for point light and attenuation calc)
	float3 reflection;//for specular calculation
	float4 textureColour;//sampled pixel from texture
	float4 diffuseComponent;//diffuse colour
	float4 specularComponent;//specular highlights
	float attenuation;//fall off factor
	float4 finalColour;//final output

	float4 watercolour = { 0.0f, 0.6f, 0.90f, 0.0f };

	// Set the default output color to the ambient light value for all pixels.
	finalColour = ambientColour[0];
	//add texture pixel colour
	finalColour = finalColour * watercolour;

	//for each light
	for (int i = 0; i < N_OF_LIGHTS; ++i)
	{
		//normalize distance of pixel from light source to get the direction
		lightDir = normalize(input.position3D - lightPosition[i]);
		// Calculate the amount of light on this pixel
		lightIntensity = saturate(dot(input.normal, -lightDir));
		//if the pixel is affected by light, calculate and add diffuse and specular components
		if (lightIntensity > 0.0f)
		{
			//diffuse
			// Determine the final diffuse colour based on the diffuse color and the amount of light intensity.
			diffuseComponent = diffuseColour[i] * lightIntensity;

			//specular
			//get reflection vector
			reflection = reflect(lightDir, input.normal);
			//get specular intensity based on reflection vector
			specularIntensity = pow(saturate(dot(reflection, input.viewDirection)), specularPower[i].x);
			//determine specular comp�onent based on specular colour and specular intensity
			specularComponent = specularColour[i] * specularIntensity;

			//attenuation
			//reduce intensity based on distance
			//linear attenuation factor = 1 - (distanceFromLight/fallOffDistance)
			//saturate value because fallOffDistance could be bigger than distanceFromLight
			attenuation = 1.0f - saturate(length(input.position3D - lightPosition[i]) / fallOffDistance[i].x);
			finalColour += (diffuseComponent + specularComponent)*attenuation;
		}
	}

	// Saturatefinal colour.	
	saturate(finalColour);
	finalColour.w = 0.1;
	return finalColour;
}